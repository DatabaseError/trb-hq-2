Actor RedSparkSpawner : SwitchableDecoration 1024
{
  Height 8
  Radius 4
  +NoBlockMap
  +NoGravity
  +NoInteraction
  +NoClip
  States
  {
  Spawn:
    TNT1 A 0
    TNT1 A 0 A_JumpIf(Args[2] > 0, "Inactive")
    TNT1 A 10 A_Jump(Args[1]/16, "Active")
    Loop
  Active:
    TNT1 A 0
    TNT1 A 0 A_PlaySound("World/Spark")
    TNT1 A 0 A_Jump(256, 1, 2, 3, 4, 5, 6)
    TNT1 A 1 A_SpawnItemEx("SparkFlare_R1", 0, 0, 0, 0, 0, 0, 0, 128)
    Goto SpawnSparks
    TNT1 A 1 A_SpawnItemEx("SparkFlare_R2", 0, 0, 0, 0, 0, 0, 0, 128)
    Goto SpawnSparks
    TNT1 A 1 A_SpawnItemEx("SparkFlare_R3", 0, 0, 0, 0, 0, 0, 0, 128)
    Goto SpawnSparks
    TNT1 A 1 A_SpawnItemEx("SparkFlare_R4", 0, 0, 0, 0, 0, 0, 0, 128)
    Goto SpawnSparks
    TNT1 A 1 A_SpawnItemEx("SparkFlare_R5", 0, 0, 0, 0, 0, 0, 0, 128)
    Goto SpawnSparks
    TNT1 A 1 A_SpawnItemEx("SparkFlare_R6", 0, 0, 0, 0, 0, 0, 0, 128)
    Goto SpawnSparks
  SpawnSparks:
    TNT1 A 0 A_JumpIf(Args[0] == 1, "SpawnDown")
    TNT1 A 0 A_JumpIf(Args[0] > 1, "SpawnSide")
    Goto SpawnUp
  SpawnUp:
    TNT1 AA 0 A_CustomMissile("Spark_R1", 0, 0, Random(0, 360), 2, Random(67, 113))
    TNT1 AA 0 A_CustomMissile("Spark_R2", 0, 0, Random(0, 360), 2, Random(67, 113))
    TNT1 AA 0 A_CustomMissile("Spark_R3", 0, 0, Random(0, 360), 2, Random(67, 113))
    TNT1 AA 0 A_CustomMissile("Spark_R4", 0, 0, Random(0, 360), 2, Random(67, 113))
    TNT1 AA 0 A_CustomMissile("Spark_R5", 0, 0, Random(0, 360), 2, Random(67, 113))
    TNT1 AA 0 A_CustomMissile("Spark_R6", 0, 0, Random(0, 360), 2, Random(67, 113))
    TNT1 AA 0 A_CustomMissile("Spark_R7", 0, 0, Random(0, 360), 2, Random(67, 113))
    TNT1 AA 0 A_CustomMissile("Spark_R8", 0, 0, Random(0, 360), 2, Random(67, 113))
    TNT1 AA 0 A_CustomMissile("Spark_R9", 0, 0, Random(0, 360), 2, Random(67, 113))
    TNT1 AA 0 A_CustomMissile("Spark_R10", 0, 0, Random(0, 360), 2, Random(67, 113))
    TNT1 AA 0 A_CustomMissile("Spark_R11", 0, 0, Random(0, 360), 2, Random(67, 113))
    TNT1 AA 0 A_CustomMissile("Spark_R12", 0, 0, Random(0, 360), 2, Random(67, 113))
    TNT1 AA 0 A_CustomMissile("Spark_R13", 0, 0, Random(0, 360), 2, Random(67, 113))
    TNT1 AA 0 A_CustomMissile("Spark_R14", 0, 0, Random(0, 360), 2, Random(67, 113))
    TNT1 AA 0 A_CustomMissile("Spark_R15", 0, 0, Random(0, 360), 2, Random(67, 113))
    TNT1 AA 0 A_CustomMissile("Spark_R16", 0, 0, Random(0, 360), 2, Random(67, 113))
    TNT1 A 1 A_JumpIf(Args[2] > 0, 1)
    Goto Spawn
    TNT1 A 1
    Goto Inactive
  SpawnDown:
    TNT1 AA 0 A_CustomMissile("Spark_R1", 0, 0, Random(0, 360), 2, Random (-67, -113))
    TNT1 AA 0 A_CustomMissile("Spark_R2", 0, 0, Random(0, 360), 2, Random (-67, -113))
    TNT1 AA 0 A_CustomMissile("Spark_R3", 0, 0, Random(0, 360), 2, Random (-67, -113))
    TNT1 AA 0 A_CustomMissile("Spark_R4", 0, 0, Random(0, 360), 2, Random (-67, -113))
    TNT1 AA 0 A_CustomMissile("Spark_R5", 0, 0, Random(0, 360), 2, Random (-67, -113))
    TNT1 AA 0 A_CustomMissile("Spark_R6", 0, 0, Random(0, 360), 2, Random (-67, -113))
    TNT1 AA 0 A_CustomMissile("Spark_R7", 0, 0, Random(0, 360), 2, Random (-67, -113))
    TNT1 AA 0 A_CustomMissile("Spark_R8", 0, 0, Random(0, 360), 2, Random (-67, -113))
    TNT1 AA 0 A_CustomMissile("Spark_R9", 0, 0, Random(0, 360), 2, Random (-67, -113))
    TNT1 AA 0 A_CustomMissile("Spark_R10", 0, 0, Random(0, 360), 2, Random (-67, -113))
    TNT1 AA 0 A_CustomMissile("Spark_R11", 0, 0, Random(0, 360), 2, Random (-67, -113))
    TNT1 AA 0 A_CustomMissile("Spark_R12", 0, 0, Random(0, 360), 2, Random (-67, -113))
    TNT1 AA 0 A_CustomMissile("Spark_R13", 0, 0, Random(0, 360), 2, Random (-67, -113))
    TNT1 AA 0 A_CustomMissile("Spark_R14", 0, 0, Random(0, 360), 2, Random (-67, -113))
    TNT1 AA 0 A_CustomMissile("Spark_R15", 0, 0, Random(0, 360), 2, Random (-67, -113))
    TNT1 AA 0 A_CustomMissile("Spark_R16", 0, 0, Random(0, 360), 2, Random (-67, -113))
    TNT1 A 1 A_JumpIf(Args[2] > 0, 1)
    Goto Spawn
    TNT1 A 1
    Goto Inactive
  SpawnSide:
    TNT1 AA 0 A_CustomMissile("Spark_R1", 0, 0, Random(-23, 23), 2, Random(157, 203))
    TNT1 AA 0 A_CustomMissile("Spark_R2", 0, 0, Random(-23, 23), 2, Random(157, 203))
    TNT1 AA 0 A_CustomMissile("Spark_R3", 0, 0, Random(-23, 23), 2, Random(157, 203))
    TNT1 AA 0 A_CustomMissile("Spark_R4", 0, 0, Random(-23, 23), 2, Random(157, 203))
    TNT1 AA 0 A_CustomMissile("Spark_R5", 0, 0, Random(-23, 23), 2, Random(157, 203))
    TNT1 AA 0 A_CustomMissile("Spark_R6", 0, 0, Random(-23, 23), 2, Random(157, 203))
    TNT1 AA 0 A_CustomMissile("Spark_R7", 0, 0, Random(-23, 23), 2, Random(157, 203))
    TNT1 AA 0 A_CustomMissile("Spark_R8", 0, 0, Random(-23, 23), 2, Random(157, 203))
    TNT1 AA 0 A_CustomMissile("Spark_R9", 0, 0, Random(-23, 23), 2, Random(157, 203))
    TNT1 AA 0 A_CustomMissile("Spark_R10", 0, 0, Random(-23, 23), 2, Random(157, 203))
    TNT1 AA 0 A_CustomMissile("Spark_R11", 0, 0, Random(-23, 23), 2, Random(157, 203))
    TNT1 AA 0 A_CustomMissile("Spark_R12", 0, 0, Random(-23, 23), 2, Random(157, 203))
    TNT1 AA 0 A_CustomMissile("Spark_R13", 0, 0, Random(-23, 23), 2, Random(157, 203))
    TNT1 AA 0 A_CustomMissile("Spark_R14", 0, 0, Random(-23, 23), 2, Random(157, 203))
    TNT1 AA 0 A_CustomMissile("Spark_R15", 0, 0, Random(-23, 23), 2, Random(157, 203))
    TNT1 AA 0 A_CustomMissile("Spark_R16", 0, 0, Random(-23, 23), 2, Random(157, 203))
    TNT1 A 1 A_JumpIf(Args[2] > 0, 1)
    Goto Spawn
    TNT1 A 1
    Goto Inactive
  Inactive:
    TNT1 A -1
    Loop
  }
}

Actor SparkFlare_W1
{
  Height 0
  Radius 0
  Mass 0
  +Missile
  +NoBlockMap
  +NoClip
  +NoGravity
  +NoInteraction
  RenderStyle Add
  Scale 0.25
  States
  {
  Spawn:
    SPKW A 1 Bright A_FadeOut(0.1)
    Loop
  }
}


Actor SparkFlare_R1 : SparkFlare_W1 { States { Spawn: SPKR A 1 Bright A_FadeOut (0.1) Loop } }
Actor SparkFlare_R2 : SparkFlare_W1 { States { Spawn: SPKR B 1 Bright A_FadeOut (0.1) Loop } }
Actor SparkFlare_R3 : SparkFlare_W1 { States { Spawn: SPKR C 1 Bright A_FadeOut (0.1) Loop } }
Actor SparkFlare_R4 : SparkFlare_W1 { States { Spawn: SPKR D 1 Bright A_FadeOut (0.1) Loop } }
Actor SparkFlare_R5 : SparkFlare_W1 { States { Spawn: SPKR E 1 Bright A_FadeOut (0.1) Loop } }
Actor SparkFlare_R6 : SparkFlare_W1 { States { Spawn: SPKR F 1 Bright A_FadeOut (0.1) Loop } }

Actor Spark_W1
{
  Height 1
  Radius 2
  Mass 0
  Speed 0.25
  +Missile
  +NoBlockMap
  +LowGravity
  RenderStyle Add
  Scale 0.025
  States
  {
  Spawn:
    SPKW E 1 Bright
    Loop
  Death:
    SPKW E 1 Bright A_FadeOut(0.1)
    Loop
  }
}


Actor Spark_R1 : Spark_W1
{
  States
  {
  Spawn:
    SPKR E 1 bright
    Loop
  Death:
    SPKR E 1 bright A_FadeOut (0.1)
    Loop
  }
}

Actor Spark_R2 : Spark_R1  { Speed 0.5  }
Actor Spark_R3 : Spark_R1  { Speed 0.75 }
Actor Spark_R4 : Spark_R1  { Speed 1.0  }
Actor Spark_R5 : Spark_R1  { Speed 1.25 }
Actor Spark_R6 : Spark_R1  { Speed 1.5  }
Actor Spark_R7 : Spark_R1  { Speed 1.75 }
Actor Spark_R8 : Spark_R1  { Speed 2.0  }
Actor Spark_R9 : Spark_R1  { Speed 2.25 }
Actor Spark_R10 : Spark_R1 { Speed 2.5  }
Actor Spark_R11 : Spark_R1 { Speed 2.75 }
Actor Spark_R12 : Spark_R1 { Speed 3.0  }
Actor Spark_R13 : Spark_R1 { Speed 3.25 }
Actor Spark_R14 : Spark_R1 { Speed 3.5  }
Actor Spark_R15 : Spark_R1 { Speed 3.75 }
Actor Spark_R16 : Spark_R1 { Speed 4.0  }


Actor Spark_Y1 : Spark_W1
{
  States
  {
  Spawn:
    SPKY E 1 bright
    Loop
  Death:
    SPKY E 1 bright A_FadeOut (0.1)
    Loop
  }
}
