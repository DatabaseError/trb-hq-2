actor grenade7 : CustomInventory
{
  radius 20
  height 26
  inventory.pickupmessage "You got a grenade throw 1."
  inventory.maxamount 30
  inventory.icon "SSBGA0"
  +ALWAYSPICKUP
  +INVENTORY.INVBAR
  states
  {
  Spawn:
    THRW ABCDEFGH 1
    loop
  Use:
    TNT1 A 0 A_Throwgrenade("Grenade5")
    stop
  }
}


actor grenade5
  {
  Projectile
  Speed 15
  Damage 18
  height 13
  radius 5
  bouncecount 10
  +DOOMBOUNCE
  -NOGRAVITY
  States
  {
  Spawn:
  THRW ABCDEFGH 5
  loop
  Death:
  THRW F 60
  BOOM F 0 A_Playsound("weapons/rocklx",CHAN_BODY,1.5,0,0.5)
  BOOM A 3 A_Explode(192,192)
  BOOM BCDEFGHIJKLMNOPQRSTUVWXY 3 bright
  BOOM F 0 
  Stop
  }
}

actor ballbfg : CustomInventory 30010
{
  radius 20
  height 26
  inventory.pickupmessage "You got a one-shot bfg."
  inventory.maxamount 70
  inventory.icon "ARTIBFGB"
  +ALWAYSPICKUP
  +INVENTORY.INVBAR
  +FLOATBOB
  states
  {
  Spawn:
    BFS2 B 1 Bright
    loop
  Use:
    TNT1 A 0 A_firecustommissile("bfgball",8,0,0)
    TNT1 A 0 A_firecustommissile("bfgball",0,0,0)
    TNT1 A 0 A_firecustommissile("bfgball",-8,0,0)
    stop
  }
}

actor greenquartz : healthpickup 82
{
  Game Raven
  SpawnID 24
  Health 10
  +COUNTITEM
  +FLOATBOB
  +INVENTORY.PICKUPFLASH
  +INVENTORY.FANCYPICKUPSOUND
  Inventory.Icon GTN2A0
  Inventory.PickupSound "ARTIUP"
  Inventory.useSound "ARTIUSE"
  Inventory.PickupMessage "Picked up some health"
  HealthPickup.Autouse 1
  States
  {
  Spawn:
    GTN2 A 4
    Loop
  }
}

