//cybruiser sounds
monster/brusit dsbrusit

monster/brudth dsbrudth

monster/bruwlk dsbruwlk
monster/brufir dsbrufir
weapons/hellex dshellex
DSCLA2 DSCLA2
//skull attacks
skull1 skull1
skull2 skull2
skull3 skull3
skull4 skull4
skull5 skull5
skull6 skull6
//flaregun weapon sound
missile/flareshot DSFLARGU
weapons/flareopen DSFLROPN
weapons/flareload DSFLRLD
weapons/flareclose DSFLRCLS

//dog sound
monsterdog/attack DOGATTAC
monsterdog/death DSBDOOF
Monsterdog/taunt DSBDNOWA
//demolisher sound

monster/demsit dsdemsit
monster/demdth dsdemdth

//realistic sounds
DISCO DISCO
WFALL WFALL
DSWATER DSWATER
// Blood zombie sounds
bloodmelee		DSZOMSWG
blooddeath		DSZOMDTH

$ambient 1 WFALL POINT CONTINUOUS 1.0
$ambient 2 DISCO POINT CONTINUOUS 1.0
$ambient 3 DSWATER POINT CONTINUOUS 1.0
$ambient 4 stream point continuous 999.1

//terrain sounds
world/drip			drip
world/watersplash		watersp
world/sludgegloop		inmuck
world/lavasizzle		innuke
world/hotsiz                    innuke
world/lavagloop			inlava
world/bloodgloop		inblud
world/nukagegloop		innuke

//Tank sounds
ZTank/see DSZTANK1
ZTank/attack DSZTGUN
ZTank/pain DSZTANKP
ZTank/death DSZTANKD
ZTank/Run DSZTANK2
ZTank/explode DSZTANKX
//classes sounds

$playeralias    Dog2     male        *Taunt       monsterdog/taunt
$playeralias    Cybruiser2     male        *Taunt     monster/brusit   
$playeralias    Tank     male        *Taunt       ZTank/see
$playeralias    Ironlich22     male        *Taunt       ironlich/sight
$playeralias    Mindheaded     male        *Taunt       DSSPISI2
$playeralias    Arachnotron9     male        *Taunt       Baby/taunt
$playeralias    sodguy1    male        *Taunt    sod/Sight
$playeralias    baron    male        *Taunt baron/sight
$playeralias    Imp    male        *Taunt imp/sight
//kescharge sound
KESCharge P_GRAVUP
KESShot P_GRAVFI
KESBoom P_GRAVHT

//chat sounds
$random misc/chat {Radio radio2 radio3 radio4 radio5}
radio dsradio
radio2 dsradi2
radio3 dsradi3
radio4 dsradi4
radio5 badmout5

//pistol
dualsfire p_a2pist
singlesfire p_apistl

//Repair gun sound
REPAIRGUN PAIRGN

//key sounds
misc/k_pkup DSKEYUP

//Cash sound
DSCASH DSCASH

//iron lich sounds

ironlich/attack1 HEDAT1
ironlich/attack2 HEDAT2
ironlich/attack3 HEDAT3
ironlich/pain HEDPAI
ironlich/sight HEDSIT
ironlich/active HEDACT

//storm
$random thunder/hit { thunder/hit1 thunder/hit2 }
thunder/hit1 THNDS01
thunder/hit2 THNDS02



//spidermastermind from twzone2
DSPISTO2 DSPISTO2
DSDMPAI2 DSDMPAI2
DSSPISI2 DSSPISI2
DSMETAL2 DSMETAL2
DSSPIDT2 DSSPIDT2


//Arachnotron sounds

Baby/taunt DSBSPSI2
DSBSPAC2 DSBSPAC2
DSBSPDT2 DSBSPDT2
baby/active2 DSBSPAC2
baby/act DSBSPWL2

//warriors room
//world/amb6			amb6
//world/amb7			amb7
world/amb8			amb8
//world/amb68			amb68
//world/amb69			amb69

//$ambient 6 world/amb6 POINT CONTINUOUS 1.0
//$ambient 7 world/amb7 POINT CONTINUOUS 1.0
$ambient 8 world/amb8 POINT CONTINUOUS 1.0
//$ambient 68 world/amb68 POINT CONTINUOUS 1.0
//$ambient 69 world/amb69 POINT CONTINUOUS 1.0

JetPack/Activate Dsjet
//warriors sounds
$playersound	"Warrior"	male	*death		dstlsdth
$playersound	"Warrior"	male	*xdeath		dstlsdhi
$playersound	"Warrior"	male	*gibbed		dstlsdth
$playersound	"Warrior"	male	*pain100	dstlspai
$playersounddup	"Warrior"	male	*pain75		*pain100
$playersounddup	"Warrior"	male	*pain50		*pain100
$playersounddup	"Warrior"	male	*pain25		*pain100
$playersound	"Warrior"	male	*grunt		dstlsoof
$playersound	"Warrior"	male	*land		dstlsoof
$playersound	"Warrior"	male	*jump		dstlsjmp
$playersound	"Warrior"	male	*fist		dspunch
$playersound	"Warrior"	male	*usefail	dstlsoof
$playeralias	"Warrior"	male	*taunt		badmout/taunt

$random badmout/taunt	{badmout/taunt1 badmout/taunt2 badmout/taunt3 badmout/taunt4 badmout/taunt5 badmout/taunt6 badmout/taunt7 badmout/taunt8}
badmout/taunt1	badmout1
badmout/taunt2	badmout2
badmout/taunt3	badmout3
badmout/taunt4	badmout4
badmout/taunt5	badmout5
badmout/taunt6	badmout6
badmout/taunt7	badmout7
badmout/taunt8	badmout8

//steam sounds
Steam/Loop	STEMLOOP
Steam/Fire	STEMFIRE

//microsoft sam
mycock mycock
intheass intheass
smtit smtit
gijgij gijgij
FAGSPHRE FAGSPHRE
dickcock dickcock
$random samlol { dickcock FAGSPHRE gijgij smtit mycock intheass }
$playeralias cock male *taunt samlol
$playeralias cock2 male *taunt samlol

tourettes/pain1 dstgpai1
tourettes/pain2 dstgpai2
$random tourettes/pain { tourettes/pain1 tourettes/pain2 }

//Touretts guy

tourettes/death1       DSTGDTH1
tourettes/death2       DSTGDTH2
tourettes/death3       DSTGDTH3
tourettes/death4       DSTGDTH4
tourettes/death5       DSTGDTH5
tourettes/death6       DSTGDTH6
tourettes/death7       DSTGDTH7
tourettes/death8       DSTGDTH8
tourettes/death9	   DSTGDTH9
tourettes/death10	   DSTGDT10
tourettes/death11	   DSTGDT11
tourettes/death12	   DSTGDT12
tourettes/death13	   DSTGDT13
tourettes/death14	   DSTGDT14
tourettes/death15	   DSTGDT15
tourettes/death16	   DSTGDT16
tourettes/death17	   DSTGDT17
tourettes/death18	   DSTGDT18
tourettes/death19	   DSTGDT19
tourettes/death20	   DSTGDT20
tourettes/death21	   DSTGDT21
tourettes/death22	   DSTGDT22
tourettes/death23	   DSTGDT23
tourettes/death24	   DSTGDT24
tourettes/death25	   DSTGDT25
tourettes/death26          DSTGDIEH
tourettes/death27          dstgpai2
$random tourettes/death { tourettes/death1 tourettes/death2 tourettes/death3 tourettes/death4 tourettes/death5 tourettes/death6 tourettes/death7 tourettes/death8 tourettes/death9 tourettes/death10 tourettes/death11 tourettes/death12 tourettes/death13 tourettes/death14 tourettes/death15 tourettes/death16 tourettes/death17 tourettes/death18 tourettes/death19 tourettes/death20 tourettes/death21 tourettes/death22 tourettes/death23 tourettes/death24 tourettes/death25 tourettes/death26 tourettes/death27 }

tourette/taunt1       DSTGTN01
tourette/taunt2       DSTGTN02
tourette/taunt3       DSTGTN03
tourette/taunt4       DSTGTN04
tourette/taunt5       DSTGTN05
tourette/taunt6       DSTGTN06
tourette/taunt7       DSTGTN07
tourette/taunt8       DSTGTN08
tourette/taunt9       DSTGTN09
tourette/taunt10      DSTGTN10
tourette/taunt11      DSTGTN11
tourette/taunt12      DSTGTN12
tourette/taunt13      DSTGTN13
tourette/taunt14      DSTGTN14
tourette/taunt15      DSTGTN15
tourette/taunt16      DSTGTN16
tourette/taunt17      DSTGTN17
tourette/taunt18      DSTGTN18
tourette/taunt19      DSTGTN19
tourette/taunt20      DSTGTN20
tourette/taunt21      DSTGTN21
tourette/taunt22      DSTGTN22
tourette/taunt23      DSTGDT20
tourette/taunt24      DSTGDTH9
tourette/taunt25          DSTGDTH5
tourette/taunt26          DSTGDTH9
$random tourette/taunt { tourette/taunt1 tourette/taunt2 tourette/taunt3 tourette/taunt4 tourette/taunt5 tourette/taunt6 tourette/taunt7 tourette/taunt8 tourette/taunt9 tourette/taunt10 tourette/taunt11 tourette/taunt12 tourette/taunt13 tourette/taunt14 tourette/taunt15 tourette/taunt16 tourette/taunt17 tourette/taunt18 tourette/taunt19 tourette/taunt20 tourette/taunt21 tourette/taunt22 tourette/taunt23 tourette/taunt24 tourette/taunt25 tourette/taunt26 }

$playeralias	Sgt.Tourette	male	*taunt		tourette/taunt
$playeralias	Sgt.Tourette	male	*death		tourettes/death
$playersound    Sgt.Tourette    male    *xdeath         dstgdieh
$playeralias	Sgt.Tourette	male	*pain100	tourettes/pain
$playersound    Sgt.Tourette	male	*pain75		dstgpai3
$playersound    Sgt.Tourette	male	*pain50		dstgpai4
$playersound    Sgt.Tourette  	male	*pain25		dstgpai5
$playersound	Sgt.Tourette	male	*land		dstgoof
$playersound	Sgt.Tourette	male	*usefail	dstgnowa

//fatso sound sight
$playeralias    Fatso     male    *Taunt      fatso/sight

//Shotgunguy sounds
$playeralias    shotgunguy      male    *Taunt    shotguy/sight

doom/pai2 DSPOPA2

bull/sight DST8CHAT
bull/death DSSWDIE1
bull/Pain DSD3DOOF
bull/active DS800ACT
bull/attacksound dsztgun


//bull fighter sounds
$playeralias	Bullfighter	male	*taunt		bull/sight
$playeralias	Bullfighter	male	*death		bull/death
$playersound    Bullfighter     male    *Pain100      DSD3DOOF
$playersound    Bullfighter     male    *Pain75      DSD3DOOF
$playersound    Bullfighter     male    *Pain50      DSD3DOOF
$playersound    Bullfighter     male    *Pain25      DSD3DOOF

//awesomeclaw sounds
BLAST1 MAGICMIS
BLAST2 METEORBA
BLAST3 RIPPERFI
BLAST4 SPHEREGR
BLAST5 RIPPERIM
signal signal

//sodguy (mechguy) sounds
sod/Sight DSBO7SIT
sod/death DSBO7DTH

attacksound/tank DSSHOT

Wolfrocket/attacksound DSMISSF
Wolfrocket/deathsound DSMISSX

//Metroid Sounds
metroid/attack METTATAK
Metroid/death METTDTH
Metroid/Pain METTPAIN
$Random Idle1 {metroid/idl1 metroid/idl2 metroid/idl3 metroid/idl4 metroid/idl5}
metroid/idl1 METTIDLE
metroid/idl2 METIDLE2
metroid/idl3 METIDLE3
metroid/idl4 MWANDER
metroid/idl5 MWANDE2

// GameNerd64 sounds for his FSR room
door3_start			d3open
door3_mid			d3move
door3_end			d3stop
$ambient 9 pfhorgoo point 2.0 continuous 75
$random pfhorgoo { goo1 goo2 goo3 }
goo1 goo1
goo2 goo2
goo3 goo3
marathon marathon

//Sodmachine
SODMACHI SODMACHI
DSSHOT DSSHOT

//sounds
ARTIUSE ARTIUSE
ARTIUP ARTIUP
PINGAS/Explode			PINGAS
PINGAS/Click		PINGASA
PINGAS/Throw			PINGASB
PINGAS/Bounce			PINGASC
Pistol Pistol

//uacbot sound
uacbsee  uacbsee
uacbact  uacbact
uacbdth  dsbarexp
2renpop  2renpop
2renfire 2renfire
2ren     2ren
2renbnce 2renbnce

//bfgguy sounds
$playeralias    Bfgguy      male    *Taunt    chainguy/sight
$playeralias	Bfgguy	male	*death		chainguy/death
$playersound    Bfgguy   male    *xdeath         dstgdieh
$playeralias	Bfgguy	male	*pain100	chainguy/pain
$playersound    Bfgguy	male	*pain75		chainguy/pain
$playersound    Bfgguy	male	*pain50		chainguy/pain
$playersound    Bfgguy  	male	*pain25		chainguy/pain
$playersound	Bfgguy	male	*land		chainguy/active
$playersound	Bfgguy	male	*usefail	chainguy/active

//revenant sound

$playeralias    Revenant     male    *Taunt    skeleton/sight
rev/punch DSSKEPC1
rev/punch2 DSSKESW1

//caco sight sound
$playeralias    cacodemon     male    *Taunt    Caco/sight

//Alarm sound
DSALARM DSALARM

weapons/vilesht1                dsrevi01
weapons/vilesht2                dsrevi02

//Baron and doomimp sound
baron/melee DSCLAW2

$random deepone/pain { deepone/hurt1 deepone/hurt2 deepone/hurt3 deepone/hurt4 }

deepone/hurt1 	DEEPPN1
deepone/hurt2 	DEEPPN2
deepone/hurt3  	DEEPPN3
deepone/hurt4 	DEEPPN4

$random deepone/active { deepone/groan1 deepone/groan2 }

deepone/groan1 	DEEPGN1
deepone/groan2 	DEEPGN2

$random deepone/meleegrowl { deepone/attack1 deepone/attack2 }

deepone/melee		DSCLAW

deepone/attack1 	DEEPAK1
deepone/attack2 	DEEPAK2

deepone/sight 		DEEPSEE
deepone/stealth		DEEPSTTH

deepone/death 	DEEPDTH

deepone/fire 	DEEPFIRE

deepone/firehit 	DEEPFHIT



attack/hand P_GRAVGO
Attack/death P_GRAVHT
Attack/Pickup G_PGOD




Green/death DSBO2DTH
Green/Sight dsbo2sit
Green/attack dsshot


Weapons/Flamethrower/Fire	FT_FIRE
Weapons/Flamethrower/Hit	ON_FIRE
Weapons/Flamethrower/On		FT_ON
Weapons/Flamethrower/Off	FT_OFF
Weapons/Flamethrower/Raise	FT_LOAD
Weapons/Flamethrower/Steam	FT_STEAM

lighton lighton
lightoff lightoff

scourge/rocket scarrckt
scourge/explode dshellex
scourge/machinegun enfire
scourge/railguncharge scarchrg
scourge/railgun scarrail
scourge/death scardeth
scourge/sight scarsigh
scourge/walk scarwalk
weapons/flamer dsflamer
weapons/scorch dsscrch2
weapons/onfire dsonfire

scaract1 scaract1
scaract2 scaract2
$RANDOM scourge/active { scaract1 scaract2 }

scarpain1 scarpai1
scarpain2 scarpai2
$RANDOM scourge/pain { scarpain1 scarpain2 }

siren siren

AmbientRain a_rain
//Ambdose DSDOSE

$Ambient 1 AmbientRain POINT CONTINUOUS 1.0
$Ambient 2 AmbientRain CONTINUOUS 1.0
//$Ambient 44 Ambdose POINT CONTINUOUS 0.2

Quadshotgun dsqshtgn
weapons/launch                  dscmtlnc
weapons/explosion               dscmtexp


Doorbuzz DoorBuzz