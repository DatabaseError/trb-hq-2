#!/bin/bash

## set git rev no.
if [ -f $(which git) ]
then
	GITREV=$(git rev-parse --short HEAD)
else
	echo "Git not found"
	GITREV=dev
fi

## remove old pk3
if [ -f trbhq-*.pk3 ]
then
	rm trbhq-*
fi

## Compile ACS
mkdir -p pk3data/acs/
acc/acc pk3data/WEATHER.ACS pk3data/acs/WEATHER.o
acc/acc pk3data/global.acs pk3data/acs/global.o

## check if zip is installed
if [ -f $(which zip) ]
then
	echo "Making PK3"
	cd pk3data
	zip -r ../trbhq-$GITREV.pk3 *
	cd ..
else
	echo "Zip not found. Exiting"
	return 26
fi

## Remove ACS object files
rm -rf pk3data/acs/

