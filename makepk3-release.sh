#!/bin/bash

HQVER=13

## Compile ACS
mkdir -p pk3data/acs/
acc/acc pk3data/WEATHER.ACS pk3data/acs/WEATHER.o
acc/acc pk3data/global.acs pk3data/acs/global.o

## check if zip is installed
if zip -v
then
	echo "Making PK3"
	cd pk3data
	zip -r ../trbhq-v$HQVER.pk3 *
	cd ..
else
	echo "Zip not found. Exiting"
	return 26
fi

## Remove ACS object file
rm -f pk3data/acs/global.o
