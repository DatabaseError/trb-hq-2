@echo off

REM autobuild for TRB HQ

REM move the maps out of the directory

mkdir .\temp
move .\pk3data\maps\* .\temp\


REM check if the directories exist

goto check


:missing
echo Something is missing! :(
pause
exit

:check
if not exist .\acc\acc.exe goto missing
if not exist .\7za\7za.exe goto missing
if not exist .\pk3data\ goto missing

REM compile ACS

ECHO Compiling ACS...
mkdir .\pk3data\acs
acc\acc.exe pk3data\weather pk3data\acs\weather
acc\acc.exe pk3data\global pk3data\acs\global


REM end compile ACS

REM packaging

echo Zipping to the PK3 file...
7za\7za.exe a -tzip trbhq-resource.pk3 .\pk3data\*

REM end packaging

REM remove ACS object file so next time this is ran there won't be 2 object files

del /Q .\pk3data\acs

REM move the maps back

move .\temp\* .\pk3data\maps\

echo Done! 
pause
