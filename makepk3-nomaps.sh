#!/bin/bash

## remove old pk3
rm -f trbhq-resources.pk3

## Compile ACS
mkdir -p pk3data/acs/
acc/acc pk3data/WEATHER.ACS pk3data/acs/WEATHER.o
acc/acc pk3data/global.acs pk3data/acs/global.o

## check if zip is installed
if zip -v
then
	mkdir temp
	echo "Making PK3"
	cd pk3data
	mv maps ../temp
	zip -r ../trbhq-resources.pk3 *
	mv ../temp/maps .
	cd ..
else
	echo "Zip not found. Exiting"
	return 26
fi

## Remove ACS object file
rm -f pk3data/acs/global.o
