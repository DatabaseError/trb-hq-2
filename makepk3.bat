@echo off

REM autobuild for TRB HQ

:start

REM Set the git revision number
for /f %%i in ('git rev-parse --short HEAD') do set githash=%%i

REM compile ACS

ECHO Compiling ACS...
mkdir .\pk3data\acs
acc\acc.exe pk3data\weather pk3data\acs\weather
acc\acc.exe pk3data\global pk3data\acs\global

REM end compile ACS

REM packaging

echo Zipping to the PK3 file
7za\7za.exe a -tzip trbhq-%githash%.pk3 .\pk3data\*

REM end packaging

REM remove ACS object file so next time this is ran there won't be 2 object files

del /Q .\pk3data\acs


echo Done!
pause
